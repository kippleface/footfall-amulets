/*

  ===========================================
        Copyright (c) 2019-2020 Kurt Tichy
              inspired by wifi spammer
       Copyright (c) 2018 Stefan Kremser
              github.com/spacehuhn
  ===========================================

Notes from SDK:
acket has to be the whole 802.11 packet, excluding the FCS. The length of the packet has to be longer than the minimum length of the header of 802.11 packet which is 24 bytes, and less than 1400 bytes.•Duration area is invalid for user, it will be filled in SDK.•The rate of sending packet is same as the management packet which is the same as the system rate of sending packets.•Can send: unencrypted data packet, unencrypted beacon/probe req/probe resp.•Can NOT send: all encrypted packets (the encrypt bit in the packet has to be 0, otherwise it is not supported), control packet, other management packet except unencrypted beacon/probe req/probe resp.•Only after the previous packet was sent, and the sent callback is entered, the next packet is allowed to send. Otherwise, wifi_send_pkt_freedom will return fail.

*/

// ===== Settings ===== //
const uint8_t channels[] = {11, 9, 6, 10, 1, 7, 2, 8, 4, 5, 3}; // used Wi-Fi channels (available: 1-14)

const int pps = 200;  // packets per second.

int pinVarenne = 13; // pin where the button / touch sensor is connected

// first 3 bytes of the MAC address of most common vendors..
const uint8_t vendors[51][3] = { 
{ 0xE8, 0x50, 0x8B },
{ 0x88, 0x83, 0x22 },
{ 0x80, 0x4E, 0x70 },
{ 0xF0, 0x5B, 0x7B },
{ 0x00, 0x04, 0x96 },
{ 0x88, 0x75, 0x98 },
{ 0x28, 0xE3, 0x47 },
{ 0x58, 0x48, 0x22 },
{ 0xF4, 0xF5, 0x24 },
{ 0x04, 0xD6, 0xAA },
{ 0x44, 0x6E, 0xE5 },
{ 0x3C, 0x2E, 0xFF },
{ 0x4C, 0xDD, 0x31 },
{ 0xD0, 0x04, 0x01 },
{ 0x48, 0x27, 0xEA },
{ 0xC8, 0x38, 0x70 },
{ 0xB4, 0xE1, 0xC4 },
{ 0x84, 0x98, 0x66 },
{ 0x84, 0xC7, 0xEA },
{ 0xB8, 0xD7, 0xAF },
{ 0x14, 0x9F, 0x3C },
{ 0x00, 0x87, 0x01 },
{ 0x30, 0x10, 0xB3 },
{ 0x8C, 0x2D, 0xAA },
{ 0x08, 0x78, 0x08 },
{ 0xE0, 0x5F, 0x45 },
{ 0x68, 0xC4, 0x4D },
{ 0x70, 0x28, 0x8B },
{ 0xC8, 0xD7, 0xB0 },
{ 0xE0, 0xAA, 0x96 },
{ 0x2C, 0x0E, 0x3D },
{ 0x30, 0x07, 0x4D },
{ 0xD8, 0xC4, 0x6A },
{ 0xEC, 0x9B, 0xF3 },
{ 0x5C, 0xE0, 0xC5 },
{ 0x4C, 0x66, 0x41 },
{ 0xF0, 0xD7, 0xAA },
{ 0xD4, 0x6D, 0x6D },
{ 0xD4, 0xE6, 0xB7 },
{ 0xDC, 0xBF, 0xE9 },
{ 0xD0, 0x53, 0x49 },
{ 0x34, 0x8A, 0x7B },
{ 0xD4, 0x63, 0xC6 },
{ 0x8C, 0xF5, 0xA3 },
{ 0xEC, 0x10, 0x7B },
{ 0x78, 0x00, 0x9E },
{ 0xAC, 0x5F, 0x3E },
{ 0x24, 0x92, 0x0E },
{ 0x84, 0xB5, 0x41 },
{ 0xD0, 0xD2, 0xB0 },
{ 0x24, 0x18, 0x1D }
};
// ==================== //

// ===== Includes ===== //
#include <ESP8266WiFi.h>

extern "C" {
#include "user_interface.h"
  typedef void (*freedom_outside_cb_t)(uint8 status);
  int wifi_register_send_pkt_freedom_cb(freedom_outside_cb_t cb);
  void wifi_unregister_send_pkt_freedom_cb(void);
  int wifi_send_pkt_freedom(uint8 *buf, int len, bool sys_seq);
}
// ==================== //

// run-time variables
char emptySSID[32];
uint8_t channelIndex = 0;
uint8_t macAddr[6];
uint8_t wifi_channel = 1;
uint32_t currentTime = 0;
uint32_t packetSize = 0;
uint32_t packetCounter = 0;
uint32_t attackTime = 0;
uint32_t packetRateTime = 0;
int totalMacs = 4999;
int whichMac;
int curMac;
uint8_t macAddrs[4999][6];

int modz = 1;
int pressed = 0;
int pressTime = 0;


// PROBE REQUEST STRUCTURE
// LENGTH 

// beacon frame definition
uint8_t beaconPacket[45] = {
  /*  0 - 3  */ 0x40, 0x00, 0x00, 0x00, // Type/Subtype: Probe Request!
  /*  4 - 9  */ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // Destination: broadcast
  /* 10 - 15 */ 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, // Source MAC (to randomize!)
  /* 16 - 21 */ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // Destination: BBSID (broadcast or to randomize too?)

  // Fixed parameters
  /* 22 - 23 */ 0x00, 0x00, // Fragment & sequence number

  // SSID parameters in case of unknown SSID
  /* 24 - 25 */ 0x00, 0x00, // Unknown SSID (generic probe request)

  // Rates parameters ( to be randomized too? or at least variate..)
  /* 26 - 27 */ 0x01, 0x04, // Supported rates, Tag length: 4
  /* 28 */ 0x02,  // 1 Mbps (binary multiples of 0.6 Mbps)
  /* 29 */ 0x04,  // 2
  /* 30 */ 0x0b,  // 5.5
  /* 31 */ 0x16,  // 11

  /* More rates parameters! */
  /* 32 - 33 */ 0x32, 0x08, //More supported rates, tag length: 8

  /* 34 */ 0x0c, // 6
  /* 35 */ 0x12, // 9
  /* 36 */ 0x18, // 12
  /* 37 */ 0x24, // 18
  /* 38 */ 0x30, // 24
  /* 39 */ 0x48, // 36 
  /* 40 */ 0x60, // 48
  /* 41 */ 0x6c, // 54

  // Current Channel
  /* 42 - 43 */ 0x03, 0x01, // Channel set, length: 1  
  /* 44 */      0x01       // Current Channel

  // // SSID parameters
  // /* 45 - 46 */ 0x00, 0x20, // Tag: Set SSID length, Tag length: 32
  // /* 47 - 69 */ 0x20, 0x20, 0x20, 0x20,
  // 0x20, 0x20, 0x20, 0x20,
  // 0x20, 0x20, 0x20, 0x20,
  // 0x20, 0x20, 0x20, 0x20,
  // 0x20, 0x20, 0x20, 0x20,
  // 0x20, 0x20, 0x20, 0x20,
  // 0x20, 0x20, 0x20, 0x20,
  // 0x20, 0x20, 0x20, 0x20, // SSID

  // // Supported Rates
  // /* 70 - 71 */ 0x01, 0x08, // Tag: Supported Rates, Tag length: 8
  // /* 72 */ 0x82, // 1(B)
  // /* 73 */ 0x84, // 2(B)
  //  74  0x8b, // 5.5(B)
  // /* 75 */ 0x96, // 11(B)
  // /* 76 */ 0x24, // 18
  // /* 77 */ 0x30, // 24
  // /* 78 */ 0x48, // 36
  // /* 79 */ 0x6c // 54
};


// generates random MAC
void randomMacs() {
  for( int k=0; k < totalMacs; k++){
  int randv=random(51);
  macAddrs[k][0] = vendors[randv][0];
  macAddrs[k][1] = vendors[randv][1];
  macAddrs[k][2] = vendors[randv][2];
  for (int i = 3; i < 6; i++)
    macAddrs[k][i] = random(256);
  }
}

void nextMac(){
  whichMac+=1;
  curMac=whichMac;
  if (whichMac > totalMacs){
    whichMac = 0;
  }
  for(int k=0; k<6; k++){
    macAddr[k]=macAddrs[whichMac][k];
  }
}

void prevMac(int n){
  int prev = whichMac - ( n * sizeof(channels) );
  if ( prev < 0 ){
     prev=totalMacs-prev;
  }
  for(int k=0; k<6; k++){
    macAddr[k] = macAddrs[prev][k];
  }  
  curMac=prev;
}

// goes to next channel
void nextChannel() {
    int ch= whichMac % sizeof(channels);
    if (ch != wifi_channel && ch >= 1 && ch <= 14) {
      wifi_channel = ch;
      wifi_set_channel(wifi_channel);
    }
}

//void randomRate() {
//   int randr = random(4);
//   if (randr == 3):
//   macAddr[
//}

void setup() {

  // for random generator
  randomSeed(os_random());
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  
  packetSize = sizeof(beaconPacket);
  
  // generate random mac addresses
  randomMacs();

  // start serial
  Serial.begin(115200);
  Serial.println();

  // get time
  currentTime = millis();

  // start WiFi
  WiFi.mode(WIFI_OFF);
  wifi_set_opmode(STATION_MODE);

  // set channel
  wifi_set_channel(channels[0]);
  
  Serial.println();
  Serial.println("Started \\o/");
  Serial.println();
  digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
  currentTime = millis();

  if ( modz == 0 ){
    Serial.println("bon, io vado a casa");

      WiFi.forceSleepBegin();
      delay(5000);
      WiFi.forceSleepWake();
     delay(100);
   if( digitalRead(pinVarenne) == 1 ){
     modz = 1;   
     wifi_set_opmode(STATION_MODE);
   }
  } else {
    if ( digitalRead(pinVarenne) == 1 ){
      pressed = 1;
      if ( pressTime == 0 ){
        pressTime = currentTime;
      } else {
        if ( ( currentTime - pressTime ) > 5000) {
          modz = 0;
          pressTime= 0;
          WiFi.mode( WIFI_OFF );
        }else{
        }
      }
    }else{
      pressed = 0;
      pressTime = 0;
    }

  // send out SSIDs
  if (currentTime - attackTime > ( 999 / pps ) ) {
    attackTime = currentTime;

    // temp variables
    int i = 0;
    int j = 0;
    int ssidNum = 1;
    char tmp;

    bool sent = false;
    
    // set next mac as source
    nextMac();
    // go to next channel
    nextChannel();


//   TODO: randomRate();

    // create packed with macaddr
    memcpy(&beaconPacket[10], macAddr, 6);

    // set channel for beacon frame
    beaconPacket[44] = wifi_channel;
    
    // set size of packet
    packetSize = sizeof(beaconPacket);
    
    // set starting sequence number
    int seqnr = ( ( currentTime / 1000 ) + ( macAddr[5] * macAddr[4] ) ) % 4096;
    
    // send packet!
    int count = 2 + ( curMac % 4 );
    
    for (int k=0; k<count ; k++){
      // update seqnr
      beaconPacket[23] = seqnr / 16;
      beaconPacket[22] = ((seqnr % 256)<<4)+0;
      
      packetCounter += wifi_send_pkt_freedom(beaconPacket, packetSize, 0) == 0;
      delay(5);
      seqnr+=1;
    }

    // // send more packets on same channel from older macs..
    for(int k=1;k<5;k++){
      prevMac(k);
      
      memcpy(&beaconPacket[10], macAddr, 6);
      beaconPacket[44] = wifi_channel;
      packetSize = sizeof(beaconPacket);
      int seqnr = ( ( currentTime / 1000 ) + ( macAddr[5] * macAddr[4] ) + k * 10) % 4096;

      int count = 2 + ( curMac % 4 );
      for (int k=0; k<count ; k++){
        beaconPacket[23] = seqnr / 16;
        beaconPacket[22] = ((seqnr % 256)<<4)+0;
        packetCounter += wifi_send_pkt_freedom(beaconPacket, packetSize, 0) == 0;
        delay(5);
        seqnr+=1;
      }
      
    }
      
      delay( 999 / pps ); 
  }

  // show packet-rate each second
  if (currentTime - packetRateTime > 1000) {
    packetRateTime = currentTime;
  digitalWrite(LED_BUILTIN, LOW);
  delay(50);                             // change here the LED duration!
  digitalWrite(LED_BUILTIN, HIGH);
    Serial.print("Packets/s: ");
    Serial.println(packetCounter);
    Serial.print("pressouch: ");
    Serial.println(pressed);
    Serial.print("presstime: ");
    Serial.println(currentTime - pressTime);
    packetCounter = 0;
  }
  }
}
