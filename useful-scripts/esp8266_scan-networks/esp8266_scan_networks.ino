#include "ESP8266WiFi.h"
 
void setup() {
 
  Serial.begin(115200);
 
  int numberOfNetworks = WiFi.scanNetworks();
 
  for(int i =0; i<numberOfNetworks; i++){
 
      Serial.print(String(i)+": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(": ");
      Serial.println(WiFi.RSSI(i));
 
  }
 
}
 
void loop() {}
