# DISCLAIMER

Please note this code is available here, but it is not yet published.  
Please do not publish it on social media or other search-engine tracked websites.
We are preparing a distribution of this code together with the larger context and support for organizing workshops  
Please contact us at <lookingback@riseup.net>  

# footfall-amulets

All versions of the amulet code are either for the ESP32 development board ('the black one') or for the ESP8266 board ('the blue one'). 

## variations

### always-on
the amulet is always on until the battery runs out. Switches back on when charged. One might regulate the 'packets per second' to make it last a bit longer.

### reset-onoff
simple on-off button using the small on-board reset button, the amulet switches between active stampeding and deep sleep.

### externaltouch-onoff
the amulet can be switched on and off by touching for more than 5 seconds an external capacitive touch sensor, suchs as the TTP223. The default pin for the touch is 13. If the touch sensor goes weird, it should be enough to let the amulet run out of battery.  

### pintouch-on
the amulet is active as long as the metal surface connected to one of the 'touch pins' is touched (default uses pin 13). then it goes back to sleep. Only for the ESP32 board. Threshold might need correction.

### pintouch-onoff
The amulet can be turned on and off by touching for more than 5 seconds the metal surface connected to the 'touch pin' (default is pin 13). Only for the ESP32 board, threshold might need correction.
 
## useful scripts for testing

### scan-networks
A script that just scans the wifi networks around the amulet.. to see how well the wireless is working..

